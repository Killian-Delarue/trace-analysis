type compare = Eq | Leq | Geq | Lt | Gt
                                      

type basic_cnstrnt = string*compare*string 

type cnstrnt = { message_kind : string ; basic_constraints : basic_cnstrnt list}

type generic_ast = cnstrnt list
                        
val pp_generic_ast : Format.formatter -> generic_ast -> unit
  
module type From_untype_functor =
  sig

    val check_messageKind : string -> bool
    val check_constraints : (string * compare * string) list -> bool
    val checkParsing : generic_ast -> bool

  end


module type Filtres =
  sig

    open Events

    val eventFiltre : 'a list -> 'b -> bool
    val nodeFiltre : 'a list -> event list -> event list
    val dataFiltre : 'a list -> ('b * event list) list -> ('b * event list) list
    
  end


module From_untype : From_untype_functor
     

open Events

let generateEvent time =
  let nodeId = (Random.int 2) + 1 in
  let eventType = Random.int 3 in
  match eventType with
  |0 -> let event = Punctual({timestamp = time; descr = ()}) in
        [((string_of_int nodeId ),event)]
        
  |1 -> let event = Long({start = time; stop = time+1; descr = ()}) in
        [((string_of_int nodeId ),event)]
  |2 -> let o = Random.int 2 in
        let aux msg =
          if nodeId = 1
          then
            let r  = Received({from_node = (string_of_int nodeId ); timestamp = time+1; msg = P2p_message({msg = Advertise([("test",0)])})}) in
            let event = Sent({to_node = "2"; timestamp = time; msg = msg}) in
            [((string_of_int nodeId ),event);("2",r)]
          else
            let r  = Received({from_node = (string_of_int nodeId ); timestamp = time+1; msg =msg}) in
            let event = Sent({to_node = "1"; timestamp = time; msg = msg}) in
            [((string_of_int nodeId ),event);("1",r)]
        in
        (match o with
         |0 ->
           aux (Message({msg = Message(())}))
         |1 ->
           let t = Random.int 6 in
           (match t with
            |0->
              aux (P2p_message({msg = Bootstrap}))
            |1->
              aux (P2p_message({msg = Advertise([("test",0)])}))
            |2->
              aux (P2p_message({msg = Swap_request((("test",0),"test"))}))
            |3->
              aux (P2p_message({msg = Swap_ack((("test",0),"test"))}))
            |4->
              aux (P2p_message({msg = Message(())}))
            |5->
              aux (P2p_message({msg = Disconnect}))
             
            |_ -> failwith "Event Generation Failed"
           )
         |_ -> failwith "Event Generation Failed"
        )
  |_ -> failwith "Event Generation Failed"

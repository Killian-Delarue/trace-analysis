{ open Parser }

rule decoupe = parse
  | ['\n'] { EOL }
  | eof { EOF}
  | "end" { END }
  | "." { POINT }
  | "(" { LPAR }
  | ")" { RPAR }
  | "==" {EQUAL}
  | "<=" {LEQ}
  | ">=" {GEQ}
  | ">" {GT}
  | "<" {LT}
  | "||" {OR}
  | "&&" {AND}
  | ['a'-'z' 'A'-'Z' '_' '0'-'9']+ {STRING (Lexing.lexeme lexbuf)}
  | [' ' '\t']+ { decoupe lexbuf }


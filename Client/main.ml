open Js_of_ocaml
open Js_of_ocaml_tyxml.Tyxml_js
open Js_of_ocaml_lwt
open Modele
(* open Events *) 
   
exception Empty_data

(*url de connexion au serveur*)
let url = "ws://localhost:9001"
        
let main _ =

  let receivedData = ref [] in
  let filtre = ref [] in
  
  let doc = Dom_html.document in

  (*Divisions Creation *)
  let main_div = Js_of_ocaml_tyxml.Tyxml_js.To_dom.of_div (
                     Html5.(div ~a:[a_id "main_div"] [])
                   )
  in


  let column_left = Js_of_ocaml_tyxml.Tyxml_js.To_dom.of_div (
                        Html5.(div ~a:[a_id "columnLeft"] [])
                      )
  in

  let column_right = Js_of_ocaml_tyxml.Tyxml_js.To_dom.of_div (
                         Html5.(div ~a:[a_id "columnRight"] [])
                       )
  in
  
  let status = Dom_html.createDiv doc in
  let filtreDiv = Dom_html.createDiv doc in
  let filtreRes = Dom_html.createDiv doc in
  let drawDiv = Dom_html.createDiv doc in
  let svgDiv = ref (Dom_html.createDiv doc) in

  (* Connection openning*)
  let ws = Connexion.connectionInit url receivedData in


  main_div##.style##.width := Js.string "100%";

  (* Header *)
  let header =
    let open Html5 in
    div ~a:[a_id "header"] [
        h1 ~a:[a_id "title"]
          [txt "Trace-Analysis"]
      ] ;
  in
  
  (* Connection status Display  *)
  Dom.appendChild status (Dom_html.createBr doc);
  let _ = Connexion.connectionStatusDiv doc status ws in

  (* Filtre *)
  let inputFiltre = Dom_html.createTextarea doc in
  inputFiltre##.cols := 35;
  inputFiltre##.rows := 8;

  Dom.appendChild filtreDiv (Js_of_ocaml_tyxml.Tyxml_js.To_dom.of_div (Html5.(div [h3 [txt "Filtre"]])));

  Dom.appendChild filtreDiv inputFiltre;
  let test = Dom_html.createDiv doc in
  Dom.appendChild filtreDiv test;

  Dom.appendChild filtreRes (Dom_html.createBr doc);
  Dom.appendChild filtreRes (Js_of_ocaml_tyxml.Tyxml_js.To_dom.of_div (Html5.(div [h4 [txt "Parsing Result :"]])));
  let checkFiltreParsing = Dom_html.createDiv doc in
  Dom.appendChild filtreRes checkFiltreParsing;
  let printFiltreRes = Dom_html.createDiv doc in
  Dom.appendChild filtreRes printFiltreRes;
  let checkFiltreConstraint = Dom_html.createDiv doc in
  Dom.appendChild filtreRes checkFiltreConstraint;
  
  
  
  (*Divisions d'affichage des données reçues*)
  Dom.appendChild doc##.body (Js_of_ocaml_tyxml.Tyxml_js.To_dom.of_div header);
  Dom.appendChild main_div column_left;
  Dom.appendChild main_div column_right;
  Dom.appendChild column_left (Js_of_ocaml_tyxml.Tyxml_js.To_dom.of_div (Html5.(div [h3 [txt "Connection Status"]])));
  Dom.appendChild drawDiv (Js_of_ocaml_tyxml.Tyxml_js.To_dom.of_div (Html5.(div [h3 ~a:[a_id "display_title"]  [txt "Display"]])));
  Dom.appendChild column_left status;
  Dom.appendChild column_left filtreDiv;
  Dom.appendChild column_left filtreRes;
  Dom.appendChild drawDiv !svgDiv;
  Dom.appendChild column_right drawDiv;
  Dom.appendChild doc##.body main_div;


 (*Connection evaluation*)
  let rec checkConnection ws =
    let _ = Connexion.connectionStatusDiv doc status ws in
    let%lwt () = Lwt_js.sleep 1.0 in
    checkConnection ws
  in
  
  let _ = checkConnection ws in
  
  let x = ref 10. in

  let rec control () =
    x := !x -. 0.8;
    let%lwt () = Lwt_js.sleep 0.08 in
    control ()
  in
  
  let _ = control() in
  
  (* Update Draw *)
  let rec draw () =

    (* let open Filtre_parser.ParserTypes in *)
    
    Dom.removeChild drawDiv !svgDiv;
    svgDiv := Dom_html.createDiv doc ;
    (* let dataFiltered = Filtres.dataFiltre !filtre !receivedData in *)
    let dataFiltered = [] in
    dataModele svgDiv dataFiltered x ; 
    Dom.appendChild drawDiv !svgDiv;
    let%lwt () = Lwt_js.sleep 0.05 in
    draw ()      
  in

  let _ = draw () in

  let rec filtering () =

    let open Filtre_parser in

    let rec print_filtre (f : ParserTypes.generic_ast) =
    
      let rec aux bc =
        
        match bc with
        |[] -> "" 
        |[(s1,c,s2)] ->
          (match c with
           | ParserTypes.Eq ->
              let s = " " ^ s1 ^ " Eq " ^ s2 in
              s
           | ParserTypes.Leq ->
              let s = s1 ^ " Leq " ^ s2 in
              s 
           | ParserTypes.Geq ->
              let s =  s1 ^ " Geq " ^ s2 in
              s
           | ParserTypes.Lt ->
              let s = s1 ^ " Lt " ^ s2 in
              s
           | ParserTypes.Gt ->
              let s = s1 ^ " Gt " ^ s2 in
              s)
        | (s1,c,s2)::l ->
           match c with
           | ParserTypes.Eq ->
              let s = " " ^ s1 ^ " Eq " ^ s2 ^ " ; " in
             s ^ (aux  l)
           | ParserTypes.Leq ->
              let s = s1 ^ " Leq " ^ s2 ^ " ; " in
              s ^ (aux l)
           | ParserTypes.Geq ->
              let s =  s1 ^ " Geq " ^ s2 ^ " ; " in
              s ^ (aux l)
           | ParserTypes.Lt ->
             let s = s1 ^ " Lt " ^ s2 ^ " ; " in
             s ^ (aux l)
           | ParserTypes.Gt ->
              let s = s1 ^ " Gt " ^ s2 ^ " ; " in
              s ^ (aux l)
              
      in
      
      match f with
      |[] -> "</br>"
      |m::l ->
        let s = "Messages of type : " ^ m.message_kind ^ " with constraint : " ^ (aux m.basic_constraints) ^ "</br>" in
          s ^ (print_filtre l)
          
          
    in
    
    try
      let v = Js.to_string inputFiltre##.value in
      let lexbuf = Lexing.from_string v in
      let parsingResult = Filtre_parser.Parser.s (Filtre_parser.Lexer.decoupe) lexbuf in
      
      checkFiltreParsing##.style##.color := Js.string  "#2ecc71";
      checkFiltreParsing##.innerHTML := Js.string "Parsing Checked";

      (if (ParserTypes.From_untype.checkParsing parsingResult)
      then
        let _ = checkFiltreConstraint##.style##.color := Js.string  "#2ecc71" in
        checkFiltreConstraint##.innerHTML := Js.string "Constraints valid";
      else
        let _ = checkFiltreConstraint##.style##.color := Js.string  "#f40a0a" in
        filtre := [];
        checkFiltreConstraint##.innerHTML := Js.string "Constraints invalid";
      );
        
      printFiltreRes##.innerHTML := Js.string (print_filtre parsingResult);
      let%lwt () = Lwt_js.sleep 0.1 in
      filtering ()
    with ( _ ) ->
      checkFiltreParsing##.style##.color := Js.string  "#f40a0a";
      checkFiltreParsing##.innerHTML := Js.string "Parsing failed";
      printFiltreRes##.innerHTML := Js.string "";
      checkFiltreConstraint##.innerHTML := Js.string "";
      let%lwt () = Lwt_js.sleep 0.1 in
      filtering ()
  
  in
  
  let _ = filtering () in
  
  

  let rec dataRequest () =
    let%lwt () = Lwt_js.sleep 1.0 in
    
    ws##send (Js.string "fullDataRequest");
    dataRequest ()
    
                        
  in

  let _ = dataRequest () in

  Lwt.return ()
  
let start _ =
  
  try
    ignore (Dom_html.createCanvas Dom_html.window##.document);
    let _ = main () in
    Js._true
  with Dom_html.Canvas_not_available ->
    Js._false
   
let _ = Dom_html.window##.onload := Dom_html.handler start

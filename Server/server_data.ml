module Server_data = 
  struct
    open Data_encoding
    (* open Events *)
       
    (* let data = [("1",[Long(1,2);Punctual(3);Sent("2",4,"content")]);("2",[Punctual(0);Long(1,2);Punctual(3);Received("1",5,"content")])] *)
    let data = []
             
    let encod encoding data= Json.construct encoding data 
                           
    let encodedData encoding = encod encoding data
                             
  end

type token =
  | EOL
  | EOF
  | END
  | POINT
  | LPAR
  | RPAR
  | EQUAL
  | LEQ
  | GEQ
  | LT
  | GT
  | OR
  | AND
  | STRING of (string)

val s :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> ParserTypes.generic_ast

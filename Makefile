all: usage 

usage:
	@echo "Usage : \nmake client to compile and run the client on browser \nmake server to compile and run the server \nmake parser to compile the filtre parser test \n\nUse dune build to compile everything without run or test"

server:
	(cd ./Server;dune build)
	./_build/default/Server/server.exe


client:
	(cd ./Client; dune build; sensible-browser index.html)	

parser:
	(cd ./deps/Filtre_parser/ ; ocamllex lexer.mll; ocamlyacc parser.mly; dune build)
	(cd ./tests/ ; dune build)

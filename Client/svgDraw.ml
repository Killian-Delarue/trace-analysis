open Js_of_ocaml_tyxml.Tyxml_js
   
(* Renvoie le svg Line correspondant *)
let svgLine ?mesure:(m = `Percent) ?color:(c = "black") x1 y1 x2 y2  =
  Svg.(line ~a:[a_stroke (`Color(c,None)); a_x1 (x1, Some m); a_y1 (y1, Some m); a_x2 (x2, Some m) ; a_y2 (y2, Some m)][])

(* Renvoie le svg rect correspondant *)
let svgRect ?mesure:(m = `Percent) ?color:(c = "black") x y w h =
  Svg.(rect ~a:[a_x (x, Some m); a_y (y, Some m); a_width (w, Some m); a_height (h, Some m); a_stroke (`Color(c,None)); a_fill `None][])

(* Renvoie la division associées au groupe svg contenant groupeContent *)
let svgGroup ?onmouseover:(omover = fun _ -> true) ?onmouseout:(omout = fun _ -> true) ?onclick:(onclick = fun _ -> true) groupContent =
  (Html5.(div
            [svg ~a:[Svg.a_width (100., Some `Percent); Svg.a_width (70., Some `Percent)]
            
               [Svg.(g
                       ~a:[a_stroke (`Color("black",None)); a_stroke_width (0.05, Some `Cm); a_onmouseover omover; a_onmouseout omout ; a_onclick onclick]
                       groupContent
               )]
            ]
  ))


module P2p_message =
  struct
    
    type 'msg t =
      | Bootstrap
      | Advertise of ((string*int) list)
      | Swap_request of (string*int) * string
      | Swap_ack of  (string*int) * string
      | Message of 'msg
      | Disconnect

    let encoding =
      let open Data_encoding in
      union
        ~tag_size: `Uint16
        (
          [ case
              (Tag 0x01)
              ~title: "Bootstrap"
              (obj1
                 (req "kind" (constant "Bootstrap"))
              
              )
              (function | Bootstrap -> Some (())
                        | _ -> None)
              (fun (()) -> Bootstrap);

            case
              (Tag 0x02)
              ~title: "Advertise"
              (obj2
                 (req "kind" (constant "Advertise"))
                 (req "arg" (list (tup2 string int64)))
              )
              (function | Advertise(list) ->
                           let rec aux l =
                             match l with
                             |[] -> []
                             |(s,i)::r -> (s,Int64.of_int i)::(aux r)
                           in
                           Some ((),aux list)
                        | _ -> None)
              (fun ((),list) ->
                let rec aux l =
                  match l with
                  |[] -> []
                  |(s,i)::r -> (s,Int64.to_int i)::(aux r)
                in
                Advertise(aux list));

            case
              (Tag 0x03)
              ~title: "Swap_request"
              (obj2
                 (req "kind" (constant "Swap_request"))
                 (req "arg" (tup2 (tup2 string int64) string))
              )
              (function | Swap_request((s1,i),s2) -> Some ((),((s1,Int64.of_int i),s2))
                        | _ -> None)
              (fun ((),((s1,i),s2)) -> Swap_request((s1,Int64.to_int i),s2));

            case
              (Tag 0x04)
              ~title: "Swap_ack"
              (obj2
                 (req "kind" (constant "Swap_ack"))
                 (req "arg" (tup2 (tup2 string int64) string))
              )
              (function |Swap_ack((s1,i),s2) -> Some ((),((s1,Int64.of_int i),s2))
                        | _ -> None)
              (fun ((),((s1,i),s2)) -> Swap_ack((s1,Int64.to_int i),s2));

            case
              (Tag 0x05)
              ~title: "Message"
              (obj2
                 (req "kind" (constant "Messsage"))
                 (req "arg" (unit))
              )
              (function | Message(_) -> Some ((),())
                        | _ -> None)
              (fun ((),_) -> Message());

            case
              (Tag 0x06)
              ~title: "Disconnect"
              (obj1
                 (req "kind" (constant "Disconnect"))
              )
              (function |Disconnect -> Some (())
                        | _ -> None)
              (fun (()) -> Disconnect);

            
          ]
        )
      
  end  

module Message =
  struct
    type 'msg t = Message of 'msg
  end


type comm_inner =
  | P2p_message of { msg :  unit P2p_message.t }
  | Message of { msg :  unit Message.t }

type ponctual_inner =
  unit
  
type long_inner =
  unit
  
type node_id = string

type event = |Punctual of {timestamp : int ; descr : ponctual_inner }
             |Long of {start : int; stop :int; descr : long_inner }
             |Sent of {to_node : node_id; timestamp :int;  msg:comm_inner }
             |Received of {from_node : node_id; timestamp :int;  msg:comm_inner }


module MessageType =
  struct
    
    type kind =
      | P2p_message
      | Random_message
      | Bootstrap
      | Advertise
      | Swap_request
      | Swap_ack
      | Message 
      | Disconnect
      
    let message_kind_from_string s =
      match s with
      |"P2p_message" -> Some(P2p_message)
      |"Message" -> Some(Message) 
      | _ -> None

    let constraint_from_string s =
      match s with 
      | "Bootstrap" -> Some(Bootstrap)
      | "Advertise" -> Some(Advertise)
      | "Swap_request" -> Some(Swap_request)
      | "Swap_ack" -> Some(Swap_ack)
      | "Message" -> Some(Message)
      | "Disconnect" -> Some(Disconnect)
      | _ -> None
  end                

module Repr =
  struct

    exception Shape_not_found

    type time = int

    type shape = | Rect of time*time | Line of time | Link of time*node_id*comm_inner
                                                            
    type repr = { shape : shape ; color : string}

    let eventRepr e id =
      
      match e with
      |Punctual({timestamp = t ; descr = ()}) ->
        { shape = Line(t) ; color = "blue"}
       
      |Long({start = t1; stop = t2; descr = ()}) ->
        { shape = Rect(t1,t2) ; color = "blue"}

      |Received({from_node = _; timestamp = t; msg = _}) ->
        { shape = Rect(t,t+1) ; color = "green"}
       
      |Sent({to_node = _ ; timestamp = t; msg = content}) ->
        { shape = Link(t,id,content) ; color = "red"}
    
  end
                        
                                     
type event_batch  = (node_id * event) list

let encoding_comm_inner =
  let open Data_encoding in
  union
    ~tag_size: `Uint16
    (
      [
        case
          (Tag 0x01)
          ~title: "P2p_message"
          (obj2
             (req "kind" (constant "P2p_message"))
             (req "msg" (P2p_message.encoding))
          
          )
          (function | P2p_message({msg = msg}) -> Some ((),msg)
                    | _ -> None)
          (fun ((),msg) -> P2p_message({msg = msg}));
        
        
        case
          (Tag 0x02)
          ~title: "Message"
          (obj2
             (req "kind" (constant "Message"))
             (req "arg" (unit))
          )
          (function |Message({msg = Message(())}) -> Some ((),())
                    | _ -> None)
          (fun ((),()) -> Message({msg =Message()}));

      ]
    )
  
  
let encoding_event =
  let open Data_encoding in
  union
    ~tag_size: `Uint16
    (
      [ case
          (Tag 0x01)
          ~title: "Punctual"
          ~description: "Punctual event"
          (obj2
             (req "kind" (constant "Punctual"))
             (req "timestamp" (int64))
          )
         (function | Punctual { timestamp = time ; descr = ()} -> Some ((),Int64.of_int time)
                   | _ -> None)
         (fun ((),time) -> Punctual({timestamp = Int64.to_int time ; descr = () }));
        
        case
          (Tag 0x02)
          ~title: "Long"
          ~description: "Long event"
          (obj3
             (req "kind" (constant "Long"))
             (req "timestampStart" (int64))
             (req "timestampEnd" (int64)))
          (function | Long {start = time1; stop = time2; descr = ()} -> Some ((),Int64.of_int time1,Int64.of_int time2)
                    | _ -> None)
          (fun ((),time1,time2) -> Long({start = Int64.to_int time1 ; stop = Int64.to_int time2 ; descr = () }));
        
        case
          (Tag 0x03)
          ~title: "Sent"
          ~description: "Message sent"
          (obj4
             (req "kind" (constant "Sent"))
             (req "idReceiver" (string))
             (req "timestamp" (int64))
             (req "msg" (encoding_comm_inner)))
             
          (function | Sent {to_node = id; timestamp = time;  msg = msg } -> Some((),id,Int64.of_int time,msg)
                    | _ -> None)
                       
                                                                                  
          (fun ((),id,time,msg) ->  Sent {to_node = id; timestamp = Int64.to_int time;  msg = msg }
           
          );
       
        case
          (Tag 0x04)
          ~title: "Received"
          ~description: "Message Received"
          (obj4
             (req "kind" (constant "Received"))
             (req "idSender" (string))
             (req "timestamp" (int64))
             (req "msg" (encoding_comm_inner)))
             
          (function | Received {from_node = id; timestamp = time;  msg = msg } -> Some((),id,Int64.of_int time,msg)
                    | _ -> None)
                       
                                                                                  
          (fun ((),id,time,msg) ->  Received {from_node = id; timestamp = Int64.to_int time;  msg = msg }
           
          );
        
      ]
    )

let encoding_batch =
  let open Data_encoding in
  list (tup2 string encoding_event)

   



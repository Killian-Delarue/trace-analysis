type compare = Eq | Leq | Geq | Lt | Gt
                                       [@@deriving show]

type basic_cnstrnt = string*compare*string
                                      [@@deriving show]

type cnstrnt = { message_kind : string ; basic_constraints : basic_cnstrnt list }
                 [@@deriving show]

type generic_ast = cnstrnt list
                     [@@deriving show]
                 
module type Message =
  sig

    type kind

    val message_kind_from_string : string -> kind option
    val constraint_from_string : string -> kind option

  end

module type From_untype_functor =
  sig

    val check_messageKind : string -> bool
    val check_constraints : (string * compare * string) list -> bool
    val checkParsing : generic_ast -> bool

  end


module type Filtres =
  sig

    open Events

    val eventFiltre : 'a list -> 'b -> bool
    val nodeFiltre : 'a list -> event list -> event list
    val dataFiltre : 'a list -> ('b * event list) list -> ('b * event list) list
      
  end
 
module From_untype_functor(X : Message) =
  struct

    let check_messageKind m =
      let v = X.message_kind_from_string m in
      match v with
      |None -> false
      |Some(_) -> true

    let rec check_constraints cnstrnts =
      match cnstrnts with
      |[] -> true
      |(v,comp,c)::l ->
        match (v,comp) with
        |("msg",Eq) ->
          let x = X.constraint_from_string c in
          (match x with
          |None -> false
          |Some(_) -> check_constraints l)
        |("timestamp",_) ->
          (try
            let _ = int_of_string c in
            check_constraints l
          with _ ->
            false
          )
        |_ ->
          false
                    
                      

    let rec checkParsing (f : generic_ast) =
      match f with
      |[] -> true
      |m::l ->
        if (check_messageKind m.message_kind)&&(check_constraints m.basic_constraints)
        then
          checkParsing l
        else
          false
                  

  end


module Filtres =
  struct
    
    open Events   
       
    let rec eventFiltre filtre event = 
      match filtre with
      |[] -> true
      |_::list ->
        eventFiltre list event
    let rec nodeFiltre filtre node =
      match node with
      |[] -> []
      |event::list ->
        match event with
        |Sent(_) 
         |Received(_) ->
          if (eventFiltre filtre event)
          then event::(nodeFiltre filtre list)
          else nodeFiltre filtre list
        |_ -> event::(nodeFiltre filtre list)
            
    let rec dataFiltre filtre data =
      match data with
      |[] -> []
      |(id,node)::list -> let newNode = nodeFiltre filtre node in
                          (id,newNode)::(dataFiltre filtre list)
                          
  end

let _ = Filtres.dataFiltre

module MType = Events.MessageType

module From_untype = From_untype_functor(MType)

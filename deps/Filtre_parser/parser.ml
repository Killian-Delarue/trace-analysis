type token =
  | EOL
  | EOF
  | END
  | POINT
  | LPAR
  | RPAR
  | EQUAL
  | LEQ
  | GEQ
  | LT
  | GT
  | OR
  | AND
  | STRING of (string)

open Parsing;;
let _ = parse_error;;
# 1 "parser.mly"
 open ParserTypes 
# 22 "parser.ml"
let yytransl_const = [|
  257 (* EOL *);
    0 (* EOF *);
  258 (* END *);
  259 (* POINT *);
  260 (* LPAR *);
  261 (* RPAR *);
  262 (* EQUAL *);
  263 (* LEQ *);
  264 (* GEQ *);
  265 (* LT *);
  266 (* GT *);
  267 (* OR *);
  268 (* AND *);
    0|]

let yytransl_block = [|
  269 (* STRING *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\002\000\003\000\004\000\004\000\005\000\005\000\
\005\000\005\000\005\000\000\000"

let yylen = "\002\000\
\002\000\001\000\005\000\005\000\003\000\005\000\001\000\001\000\
\001\000\001\000\001\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\000\000\012\000\000\000\000\000\000\000\001\000\
\000\000\000\000\000\000\000\000\000\000\000\000\007\000\008\000\
\009\000\010\000\011\000\000\000\004\000\003\000\000\000\000\000\
\006\000"

let yydgoto = "\002\000\
\004\000\005\000\006\000\013\000\020\000"

let yysindex = "\004\000\
\249\254\000\000\004\255\000\000\008\000\008\255\006\255\000\000\
\000\255\255\254\012\255\250\254\009\255\249\254\000\000\000\000\
\000\000\000\000\000\000\002\255\000\000\000\000\005\255\255\254\
\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\016\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\013\255\000\000\
\000\000"

let yygindex = "\000\000\
\000\000\005\000\000\000\252\255\000\000"

let yytablesize = 20
let yytable = "\015\000\
\016\000\017\000\018\000\019\000\001\000\003\000\007\000\008\000\
\009\000\010\000\011\000\012\000\014\000\021\000\023\000\002\000\
\024\000\005\000\022\000\025\000"

let yycheck = "\006\001\
\007\001\008\001\009\001\010\001\001\000\013\001\003\001\000\000\
\001\001\004\001\011\001\013\001\001\001\005\001\013\001\000\000\
\012\001\005\001\014\000\024\000"

let yynames_const = "\
  EOL\000\
  EOF\000\
  END\000\
  POINT\000\
  LPAR\000\
  RPAR\000\
  EQUAL\000\
  LEQ\000\
  GEQ\000\
  LT\000\
  GT\000\
  OR\000\
  AND\000\
  "

let yynames_block = "\
  STRING\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'constraint_list) in
    Obj.repr(
# 9 "parser.mly"
                        (_1)
# 113 "parser.ml"
               : ParserTypes.generic_ast))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'constrnt) in
    Obj.repr(
# 11 "parser.mly"
                             ([_1])
# 120 "parser.ml"
               : 'constraint_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 4 : 'constrnt) in
    let _5 = (Parsing.peek_val __caml_parser_env 0 : 'constraint_list) in
    Obj.repr(
# 12 "parser.mly"
                                        (_1::_5)
# 128 "parser.ml"
               : 'constraint_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 4 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'basic_constraints) in
    Obj.repr(
# 15 "parser.mly"
                                                    ({message_kind = _1 ; basic_constraints = _4})
# 136 "parser.ml"
               : 'constrnt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : string) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'bool_op) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 17 "parser.mly"
                                            ([(_1,_2,_3)])
# 145 "parser.ml"
               : 'basic_constraints))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 4 : string) in
    let _2 = (Parsing.peek_val __caml_parser_env 3 : 'bool_op) in
    let _3 = (Parsing.peek_val __caml_parser_env 2 : string) in
    let _5 = (Parsing.peek_val __caml_parser_env 0 : 'basic_constraints) in
    Obj.repr(
# 18 "parser.mly"
                                                ((_1,_2,_3)::_5)
# 155 "parser.ml"
               : 'basic_constraints))
; (fun __caml_parser_env ->
    Obj.repr(
# 20 "parser.mly"
                  (Eq)
# 161 "parser.ml"
               : 'bool_op))
; (fun __caml_parser_env ->
    Obj.repr(
# 21 "parser.mly"
        (Leq)
# 167 "parser.ml"
               : 'bool_op))
; (fun __caml_parser_env ->
    Obj.repr(
# 22 "parser.mly"
        (Geq)
# 173 "parser.ml"
               : 'bool_op))
; (fun __caml_parser_env ->
    Obj.repr(
# 23 "parser.mly"
       (Lt)
# 179 "parser.ml"
               : 'bool_op))
; (fun __caml_parser_env ->
    Obj.repr(
# 24 "parser.mly"
       (Gt)
# 185 "parser.ml"
               : 'bool_op))
(* Entry s *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let s (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : ParserTypes.generic_ast)

open Js_of_ocaml
open Events
open SvgDraw
   
open Js_of_ocaml_tyxml.Tyxml_js

module DataMap = Map.Make(String)


exception Not_found

        
let rec findEvent events idSender contentSent x =
  match events with
  |[] -> raise Not_found
  |e::r ->
    match e with
    |Received({from_node = id; timestamp = t; msg = msg}) ->
      if (id = idSender)&&(contentSent = msg)
      then
        (((float_of_int t)*.10. +.x),35.)
      else
        findEvent r idSender contentSent x
    |_ -> findEvent r idSender contentSent x
        
let rec findReceiver data idSender idReceiver content =
  match data with
  |[] -> raise Not_found
  |(id,events)::r -> if id = idReceiver
                     then
                       findEvent events idSender content
                     else
                       findReceiver r idSender idReceiver content


let rec constructNodeSvgList data id events x0 y =

  let open Events.Repr in 
  
  match events with
  |[] -> []
  |e::list ->
    let repr = Repr.eventRepr e id in
    match repr with
    |{ shape = Rect(t1,t2); color = c} ->
      let x1 = (float_of_int t1)*.10. +.x0 in
      let x2 = (float_of_int t2)*.10. +.x0 in
      let r = svgRect ~color:c x1 (y -. 5.) (x2 -. x1) 10. in
      r::(constructNodeSvgList data id list x0 y)
      
    |{ shape = Line(t); color = c } ->
      let x = (float_of_int t)*.10. +.x0 in
      let r = svgLine ~color:c x y x (y -. 5.) in
      r::(constructNodeSvgList data id list x0 y)

    |{ shape = Link(t,idSender,content) ; color = c } ->
      let x = (float_of_int t)*.10. +.x0 in
      let r = svgRect ~color:c x (y -. 5.) 5. 10. in
      (
        try
          let (receiverX,receiverY) = findReceiver data idSender id content x0 in
          let line = svgLine (x +.2.5) (y +. 5.) (receiverX +. 2.5) receiverY in
          line::(r::(constructNodeSvgList data id list x0 y))
        with Not_found ->
          let line = svgLine ~color:"red" (x +.2.5) (y +. 5.) (x +.2.5) (y +. 10.) in
          line::(r::(constructNodeSvgList data id list x0 y))
      )

                  


let constructDataList data x =
  let rec aux d y =
    match d with
    |[] -> []
    |(id,events)::r -> let nodeList = constructNodeSvgList data id events x y in
                       let list = aux r (y +. 30.) in
                       let line = svgLine 0. y 100. y in
                       (line::nodeList) @ list
    
  in
  aux data 10.
                 
              
let dataModele svgDiv data x = 

  let group = constructDataList data !x in
  let div = svgGroup group in
  Dom.appendChild !svgDiv (To_dom.of_div div)



                  


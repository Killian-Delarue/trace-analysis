open Js_of_ocaml
open Js_of_ocaml_tyxml.Tyxml_js
open WebSockets

let rec union l1 l2 =
  match l1 with
  |[] -> l2
  |a::reste -> if List.mem a l2
               then union reste l2
               else a::(union reste l2) 

let rec addReceivedEvent receivedData id event =
    match receivedData with
    |[] -> [(id,[event])]
    |(idNode,events)::list -> if idNode = id
                              then
                                (idNode,event::events)::list
                              else
                                (idNode,events)::(addReceivedEvent list id event)
                        
let rec addReceivedData receivedData data =
  match data with
  |[] -> ()
  |(id,event)::list-> receivedData := addReceivedEvent !receivedData id event;
                      addReceivedData receivedData list 

let  decod data =
  let dataString = Js_of_ocaml.Js.to_string data in
  let data = Result.get_ok (Data_encoding.Json.from_string dataString) in
  let batch = Data_encoding.Json.destruct Events.encoding_batch data in
  batch
                      
let connectionInit url receivedData =
  let ws = new%js webSocket (Js.string url) in
  ws##.onmessage :=
    Dom.handler(
        fun r -> let s = r##.data in
                 let data = decod s in
                 let _ = addReceivedData receivedData data in
                 Js._true
      );
  ws

(*Ferme la connection à ws*) 
let closeConnection ws =
  ws##close

(*Met à jour la div status en fonction de l'état de la conencion ws *)
let connectionStatusDiv doc status ws =
  let r = ws##.readyState in
  match r with
  | CONNECTING -> status##.style##.color := Js.string "#ff8d33";
                  status##.innerHTML := Js.string "Connecting";
                  status
  | OPEN -> status##.style##.color := Js.string  "#2ecc71";
            status##.innerHTML := Js.string "Open";
            let button = Dom_html.createButton doc in
            button##.onclick :=
              Dom_html.handler (
                  fun _ -> closeConnection ws;
                           Js._true
                );
            Dom.appendChild status (Dom_html.createBr doc);
            Dom.appendChild status (Dom_html.createBr doc);
            Dom.appendChild button (Js_of_ocaml_tyxml.Tyxml_js.To_dom.of_div (Html5.(div [txt "Déconnexion"])));
            Dom.appendChild status button;
            status
  | CLOSING -> status##.style##.color := Js.string "#ff8d33";
               status##.innerHTML := Js.string "Closing";
               status
  | CLOSED -> status##.style##.color := Js.string "#f40a0a";
              status##.innerHTML := Js.string "Closed";
              status

                
    

open Lwt.Infix
open Websocket
open Data_encoding
open Websocket_lwt_unix

let id = ref 0

open Events
open Server_data
  
exception Empty_data

(* Union de l1 et l2*)
let rec union l1 l2 =
  match l2 with
  |[] -> l1
  |a::reste -> if List.mem a l1
               then union l1 reste
               else union (a::l1) reste

let rec restriction l1 l2 =
  match l1 with
  |[] -> []
  |e::reste -> if List.mem e l2
               then restriction reste l2
               else e::(restriction reste l2) 
  

let checkRequestContent content data =
  match content with
  |"fullDataRequest" -> let j = Server_data.encod encoding_batch data in 
                        let s = Json.to_string j in
                        s     
                          
  |_ -> "Request error"


module Listener =
  struct

    let listeners = ref []
    
    let add_listener f =
      listeners := f::!listeners

    let push_event e =
      List.iter
        (fun f -> f e)
        !listeners
    
  end



let handler idClient client =

  let data = ref [] in
  
  let f (event: node_id*event) = data := event::!data in

  Listener.add_listener f;
              
  
  let id = !idClient in
  idClient := !idClient+1;
  
  let dataSent = ref [] in

  let send = Connected_client.send client in
  let _ = Lwt_log.info ("Connection with client " ^ (string_of_int id)  ^ " established") in
  let rec recv_loop () =
    let open Frame in
    let react fr =
      match fr.opcode with
      | Opcode.Ping -> Lwt_log.info ("Ping received from client " ^ (string_of_int id)) >>= fun() -> 
                       Lwt_log.info ("Pong sent to client " ^ (string_of_int id)) >>= fun() ->  
                       send @@ Frame.create ~opcode:Opcode.Pong ~content:fr.content ()

      | Opcode.Close -> Lwt_log.info ("Connection with client " ^ (string_of_int id) ^ " closed")

      | Opcode.Pong -> Lwt_log.info ("Pong received from client " ^ (string_of_int id))
                      

      | Opcode.Text
        | Opcode.Binary ->  Lwt_log.info ("Request received from client " ^ (string_of_int id) ^ " : " ^ fr.content) >>= fun() ->
                           let toSend = checkRequestContent fr.content (restriction !data !dataSent)  in
                           dataSent := union !data !dataSent;
                           if toSend = "[]"
                           then
                             Lwt_log.info ("No data to send to client " ^ (string_of_int id))
                           else
                             Lwt_log.info ("Message sent to client " ^ (string_of_int id) ^ " : " ^ toSend) >>= fun () -> 
                             send @@ Frame.create ~content: toSend ()

      | _ ->
         send @@ Frame.close 1002 >>= fun () ->
         Lwt.fail Exit
    in
    Connected_client.recv client >>= react >>= recv_loop
  in
  Lwt.catch
    recv_loop
    (fun exn ->
      Lwt_log.info ("Client " ^ (string_of_int id) ^ "failed with " ^ (Printexc.to_string exn))
    )

let main uri =

  let time = ref 0 in
  
  let rec newNode () =
    let p = Lwt_unix.sleep 2.0 in
    let l = EventGeneration.generateEvent !time in
    time := !time + 2;
    List.iter
      (fun e -> Listener.push_event e)
      l;
    Lwt.bind p (fun () -> Lwt.return () ) >>= fun _ -> 
    newNode ()
  in

  let _ = newNode () in
  
  Resolver_lwt.resolve_uri ~uri Resolver_lwt_unix.system >>= fun endp ->
  let open Conduit_lwt_unix in
  
  endp_to_server ~ctx:default_ctx endp >>= fun server ->
  
  establish_server  ~check_request:(fun _ -> true) ~ctx:default_ctx ~mode:server (handler @@ id)

let () =
  
  let uri = ref "http://localhost:9001" in

  Lwt_log.default :=
    Lwt_log.channel
      ~template:"$(date).$(milliseconds) [$(level)] $(message)"
      ~close_mode:`Close
      ~channel:Lwt_io.stdout
      ();
  Lwt_log.add_rule "*" Lwt_log.Info;              
          
  ignore @@ main @@ Uri.of_string !uri;
  Lwt_main.run (fst (Lwt.wait ()))

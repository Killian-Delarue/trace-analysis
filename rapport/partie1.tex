\section{Le projet}
\label{sec:Le projet}

\subsection{Présentation du projet}

\subsubsection{Contexte}
\paragraph{}
Le réseau de \textit{Tezos} est composé d'un ensemble de noeuds évoluant dynamiquement. Ces noeuds communiquent entre eux selon des protocoles spécifiques.\\
Il est nécessaire que les évènements ayant lieu sur les noeuds ainsi que la communication soient conformes aux attentes, en particulier aux protocoles définis.



\subsubsection{Objectifs}
L'objectif du stage est de créer un outil de visualisation des traces d'exécutions sur les noeuds.\\
Cet outil doit permettre de visualiser les évènements internes aux nœuds mais aussi (et surtout) les interactions avec les autres noeuds.\\

\paragraph{}
Le premier objectif est de mettre en place un serveur permettant de récupérer les données des nœuds, les transcoder vers une forme transmissible et utilisable pour la plate-forme de visualisation. 

\paragraph{}
Dans un second temps, nous devons créer la plate-forme en elle même. Celle-ci doit pouvoir se connecter, interagir et récupérer les données depuis le serveur.\\
Elle doit permettre de visualiser de manière claire les données reçues. Nous devons créer une représentation la plus interactive possible, afin de pouvoir efficacement visualiser les données qui nous intéressent.\\
Nous intégrerons alors une système de filtrage des données affichées par la plate-forme.\\
Ce système prendra la forme d'un langage de filtrage pouvant être entré par l'utilisateur de la plate-forme.

\paragraph{}
Pendant la durée du stage, cet outil prendra la forme d'un prototype pouvant être appliqué à n'importe quel type d'événement ayant lieu sur le réseau de noeuds.\\
Nous porterons une attention particulière à faire en sorte que l'outil soit adaptable aisément pour faciliter le branchement sur les noeuds de Tezos et le mettre à jour de manière efficace.\\
Cela ce fera par un développement clair et modulaire.

\subsubsection{Les outils de développement}
\paragraph{}
L'ensemble des éléments du projet sera développé en \textit{OCaml}, le langage de développement des protocoles de \textit{Tezos}.\\
\textit{OCaml} est un langage fonctionnel puissant, rapide et fiable. \textit{OCaml} offre à Tezos un écosystème riche et efficace pour la vérification formelle.\\
Il conviendra d'être cohérent avec le langage de développement des noeuds de \textit{Tezos} pour le développement de notre outil.

\paragraph{}
Nous utiliserons alors plusieurs librairies de \textit{OCaml} adaptées aux différents aspects du projet :

\paragraph{Js\_of\_ocaml}\footnote{https://github.com/ocsigen/js\_of\_ocaml}
La plate-forme s'exécutera sur un navigateur. Ainsi le plus gros de la visualisation se fera en Javascript.\\
Nous utiliserons donc la librairie \textit{Js\_of\_ocaml} (Jsoo) qui permet de compiler un code écrit en OCaml en un code JS.

\paragraph{Lwt}\footnote{https://github.com/ocsigen/lwt}
Lwt est une librairie de promesses.\\
Nous l'utiliserons pour créer des \textit{"threads"} à la fois au niveau du serveur et du client. 

\paragraph{Websocket}\footnote{https://github.com/vbmithr/ocaml-websocket}
La librairie \textit{Websocket} permet d'implanter une communication client-serveur selon le protocole websocket.\\
Cette librairie sera utilisée pour l'implantation du serveur du projet (la partie client de la communication pourra être réalisée grâce à \textit{Js\_of\_ocaml}.  

\paragraph{Data\_Encoding}\footnote{https://gitlab.com/nomadic-labs/data-encoding}
\textit{Data\_encoding} est un librairie pour encoder et décoder des données en \textit{OCaml}.\\
Cette librairie est maintenue par \textit{Nomadic labs} et développée principalement pour le développement du noeud Tezos.\\ 
Nous l'utiliserons pour encoder les données des noeuds au format Json afin de les transmettre depuis le serveur.


\subsection{Architecture du projet}

\subsubsection{Fonctionnement}

\paragraph{}
Le projet fonctionne grâce à une communication client-serveur.\\
Un serveur collecte les données de l'ensemble des noeuds du réseau. Dès que le serveur reçoit une requête de la part d'un client, celui-ci lui envoie un paquet correspondant à l'ensemble des données stockées par le serveur qui n'auraient pas déjà été envoyées à ce client.\\
Le modèle et la vue sont ensuite créés côté client afin de visualiser les données reçues. C'est également le client qui est responsable d'analyser syntaxiquement le filtre entré par l'utilisateur et de l'appliquer à la représentation.
\begin{figure}[hbtp]
\centering
\caption{Schéma de l'architecture}
\includegraphics[scale=0.55]{images/Diagramm.png}
\end{figure}

\subsubsection{Librairies et modules}

\paragraph{}
Le projet se décompose en trois parties :
\begin{itemize}
\item Le serveur
\item Le client 
\item Les dépendances : librairie \textit{Events} et \textit{Filtre\_Parser}
\end{itemize}

\paragraph{}
\textbf{La partie serveur} est la partie responsable du transcodage des données et de l'envoi de celles-ci vers les différents clients qui lui sont connectés.\\
L'élément important de cette partie est l'implantation serveur dans le fichier \textit{server.ml}.\\
Le prototype réalisé pendant ce stage ajoute à cette partie les module \textit{FakeNodes} et \textit{EventGeneration} dont le but est de simuler la génération d'événements par les noeuds afin de tester le bon fonctionnement du projet. 

\paragraph{}
\textbf{La partie client} est la partie dédiée à la plate-forme de visualisation.\\
Elle contient la construction de la page en elle-même (\textit{main.ml}), la création du modèle à partir des données reçues (\textit{modele.ml}), la gestion de la connexion avec le serveur \textit{connection.ml}.

\paragraph{}
\textbf{La librairie Events} contient les définitions des types de message et de la représentation graphique souhaitée par le client. Nous y trouverons les éléments nécessaires à l'encodage et au décodage des données. \\
C'est cette librairie qui sera modifiée pour adapter la plate-forme à l'utilisation voulue.

\paragraph{}
\textbf{La librairie Filtre\_Parser} fournit au projet l'analyseur syntaxique permettant de créer le filtre.\\
Elle contient le parser et le lexer correspondant au langage de filtre défini pour les données à visualiser. 

\paragraph{}
Chacune de ces parties est contenue dans un sous-dossier du projet avec un fichier \textit{dune}\footnote{https://dune.readthedocs.io/en/stable/} pour les compiler.\\
Nous rajoutons également à la racine du projet un makefile pour faciliter la compilation et l'exécution.

\begin{figure}[hbtp]
\includegraphics[scale=0.5]{images/project_diagram.png}
\caption{Arborescance du projet (simplifiée)}
\end{figure}



~


%{ open ParserTypes %}

%token EOL EOF END POINT LPAR RPAR EQUAL LEQ GEQ LT GT OR AND
%token<string> STRING
%start s
%type<ParserTypes.generic_ast> s
%%

s : constraint_list EOF {$1}

constraint_list : | constrnt {[$1]}
  | constrnt EOL OR EOL constraint_list {$1::$5}


constrnt : STRING POINT LPAR basic_constraints RPAR {{message_kind = $1 ; basic_constraints = $4}}

basic_constraints : | STRING bool_op STRING {[($1,$2,$3)]}
  | STRING bool_op STRING AND basic_constraints {($1,$2,$3)::$5}

bool_op : | EQUAL {Eq}
  | LEQ {Leq}
  | GEQ {Geq}
  | LT {Lt}
  | GT {Gt}



